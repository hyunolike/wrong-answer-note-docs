# ✔오답 노트
오답 노트(이슈 회고)


## Web
- [`react-query` `zustand` 비동기 처리 이슈](./docs/react/react-query-zustand.md)
- [`react` 화살표 함수 이슈](./docs/react/react-state.md) 
- [`react-query` refetch 요청 2번 연달아 실행되는 문제](./docs/react/react-query.md)
## Server
- [젠킨스 내 그레들 버전 일치시켜주자 ㅠ,ㅠ 그럼 빌드가 막혀.. 또한 가져오는 스프링부트 라이브러리 저장소 방화벽도 열어줘야돼!](./docs/deploy/mvn.md)
- [`SSH 연결`](./docs/ssh/ssh.md)
- [`MongoDB` 사내망에서 연결](./docs/db/mongodb.md)
- [이상한 repository 반환값](./docs/spring/repo-dto.md)
- [`jsch` 라이브러리 이용해 `한글명` 폴더 생성하는 방법](./docs/spring/korea-file-name.md)
- [스프링부트 OncePerRequestFilter 휴먼이슈 해결](./docs/spring/OncePerRequestFilter.md)
## App
- [Andorid emulator ssl 통신 설정 문제](docs/app/emulator-ssl.md)
