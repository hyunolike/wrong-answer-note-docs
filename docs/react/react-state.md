## 1. 문제 정의
```js
<MessageViewEditor
    preview={preview}
    setPreview={() => handlePreview()} ⭐
/>
```


- 위와 같이 자식 컴포넌트를 화살표 함수 이용해서 전달 시 `undefined`로 나오는 문제가 발생
## 2. 문제 해결 과정
- 화살표 함수 사용 학습
- 화살표 함수는 일반 객체 메소드 사용이 어렵다
    - `this` 가 없기에 메소를 호출한 객체를 가리키지 않고 상위 컨택스틍니 전역 객체 `window`를 가리키기 때문



```js
<MessageViewEditor
    preview={preview}
    setPreview={handlePreview} ⭐
/>
```
## 3. 아쉬운 점
- 자바스크립트 기본 베이스 부족
## 4. 개선할 점
- 자바스크립트 심화 학습
## 5. 잘한 점
