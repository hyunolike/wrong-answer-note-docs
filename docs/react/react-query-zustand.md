## 1. 문제 정의
- `react-query` `zustand` 이용해 GET api 요청 처리 시 비동기 처리로 빈값으로 input 값 들어가는 문제
- ![Alt text](image.png)
## 2. 문제 해결 과정
1. react-query 동작 파악
2. input 업데이트 시점 파악
3. react-query 심화 학습


- ![Alt text](image-1.png)
## 3. 아쉬운 점
- react-query 동작 이해 못한 점
## 4. 개선할 점
- 기본 라이브러리 사용법 숙지
## 5. 잘한 점
