## 1. 문제 정의
- `ChannelSftp.mkdir()` 이용해 한글명 폴더 생성되지 않는 문제
## 2. 문제 해결 과정
- ![](%E1%84%8B%E1%85%B2%E1%84%89%E1%85%B5%E1%86%B7%E1%84%87%E1%85%A2%E1%84%89%E1%85%A9%E1%86%BC%20%E1%84%89%E1%85%A5%E1%84%87%E1%85%B5%E1%84%89%E1%85%B3%20-%20%E1%84%92%E1%85%A1%E1%86%AB%E1%84%80%E1%85%B3%E1%86%AF%E1%84%86%E1%85%A7%E1%86%BC%20%E1%84%91%E1%85%A9%E1%86%AF%E1%84%83%E1%85%A5%20%E1%84%89%E1%85%A2%E1%86%BC%E1%84%89%E1%85%A5%E1%86%BC%20(SFTP)%402x.png)
- 문제 파악 리스트
    - 시스템 인코딩 확인
    - mkdir() 실행 시 한글 깨지는 경우 >> 적절한 인코딩 형식으로 반환


```java
// 시스템 인코딩 확인
import java.nio.charset.Charset;

public class EncodingExample {
    public static void main(String[] args) {
        String encoding = System.getProperty("file.encoding");
        System.out.println("File system encoding is: " + encoding);
        Charset charset = Charset.defaultCharset();
        System.out.println("Default charset is: " + charset.displayName());
    }
}

// 적절한 인코딩 형식 변환
String folderName = "한글폴더명";
String utf8FolderName = new String(folderName.getBytes("UTF-8"), "UTF-8");

ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
sftpChannel.connect();
sftpChannel.mkdir(utf8FolderName);
sftpChannel.disconnect();

// (추가)
Session session = jsch.getSession(username, host, port);
...
session.setConfig("PreferredAuthentications", "password,publickey");
session.setConfig("StrictHostKeyChecking", "no");
session.setConfig("PreferredCipher", "aes128-cbc");   

session.connect();
ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
String encoding = channelSftp.getSession().getServerCharset();

```
### chatgpt 답변이야 😛
```
한글 폴더명을 생성할 때 `jsch` 라이브러리를 사용하여 에러가 발생할 경우, 다음과 같은 방법으로 해결할 수 있습니다.

1. `jsch` 라이브러리의 버전 확인: 가장 먼저 사용하는 `jsch` 라이브러리의 버전을 확인해야 합니다. 구버전의 `jsch` 라이브러리에서는 한글 폴더명 생성 시 문제가 발생할 수 있습니다. 최신 버전의 `jsch` 라이브러리를 사용하는 것이 좋습니다.

2. `jsch` 라이브러리의 인코딩 설정: `jsch` 라이브러리의 인코딩을 명시적으로 설정하는 것이 도움이 될 수 있습니다. 예를 들어, `ChannelSftp` 객체의 `setFilenameEncoding()` 메서드를 사용하여 인코딩을 설정할 수 있습니다.


ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
channelSftp.connect();
channelSftp.setFilenameEncoding("UTF-8"); // 인코딩 설정


위 예제 코드에서는 `UTF-8` 인코딩을 사용하도록 설정하였습니다. 필요에 따라 다른 인코딩을 설정할 수 있습니다.

3. 서버 측 인코딩 설정: 한글 폴더명 생성에 문제가 있다면, 서버 측에서의 인코딩 설정도 확인해야 합니다. 서버의 파일 시스템이나 운영 체제에 따라 다양한 인코딩이 사용될 수 있습니다. 서버의 인코딩 설정이 올바르게 되어 있는지 확인하고 필요에 따라 서버 측 인코딩 설정을 조정해야 할 수 있습니다.

4. 파일 시스템에 대한 권한 확인: 폴더 생성 시 파일 시스템의 권한에 따라 에러가 발생할 수 있습니다. 폴더를 생성하는데 필요한 권한이 있는지 확인하고, 필요한 권한이 부여되어 있는지 확인해야 합니다.

위의 방법들을 차례로 확인하여 문제를 해결할 수 있습니다. 또한, `jsch` 라이브러리 외에도 다른 라이브러리나 방법을 사용하여 한글 폴더명 생성을 시도해 볼 수도 있습니다.
```

## 3. 아쉬운 점
## 4. 개선할 점
## 5. 잘한 점
