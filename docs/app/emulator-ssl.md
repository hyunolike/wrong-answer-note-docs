# Android Emulator SSL 통신 문제
## 1. 문제 정의
- 사내망 내에서 샌드버드 샘플앱 동작시 SSL 통신 문제
- ![image.png](./image.png)
## 2. 문제 해결 과정
- 사전 작업: SDK 빌드 성공 -> 샌드버드 SDK 통신(`https://`)
    - 자바 11 버전, 그래들 7, 안드로이드 스튜디오 버전은 최신 버전 사용해도 돼!
    - 자바 11 버전에서 ssl 인증서 등록 한다 >> sdk 다운로드 성공되면서 빌드 성공
- 이제는 에뮬레이터 내(app) ssl 통신으로 서버 접속 시도
    - [안드로이드 네트워크 보안 구성](https://developer.android.com/training/articles/security-config?hl=ko)
    - [참고 해결 자료](https://dev.to/enyason/how-to-fix-issue-of-ssl-handshake-exception-on-android-g6g)
    - 위의 참고 자료를 통해 해결 진행
- 구체적인 문제 해결 방법
    1. `crt파일` raw 폴더에 추가
    2. `xml폴더` - network_security_config.xml
    3. `Manifast.xml` 파일 추가 코드 작성

```xml
<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
    <base-config>
        <trust-anchors>
            <certificates src="@raw/kona_proxy" />
            <certificates src="system"/>
        </trust-anchors>
    </base-config>
</network-security-config>
```
- ![image-1.png](./image-1.png)
- 성공적인 화면 
    - ![image-2.png](./image-2.png)
    - ![image-3.png](./image-3.png)
- 로직
    - ![image-4.png](./image-4.png)
## 3. 아쉬운 점
## 4. 개선할 점
## 5. 잘한 점


